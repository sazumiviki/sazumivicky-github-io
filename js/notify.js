let showingAlert = !1;
const interval = setInterval(() => {
  (document.title = showingAlert
    ? "Sazumi Viki | Learn About the Tech World"
    : "Powered by sazumi Cloud"),
    (showingAlert = !showingAlert);
}, 1e3);
